/**
 * Created by derekmiranda on 11/22/13.
 */
(function($) {
    $.fn.drags = function(opt) {

        opt = $.extend({handle:"",cursor:"move"}, opt);

        if(opt.handle === "") {
            var $el = this;
        } else {
            var $el = this.find(opt.handle);
        }

        return $el.css('cursor', opt.cursor).on("mousedown", function(e) {
            if(opt.handle === "") {
                var $drag = $(this).addClass('draggable');
            } else {
                var $drag = $(this).addClass('active-handle').parent().addClass('draggable');
            }
            var z_idx = $drag.css('z-index'),
                drg_h = $drag.outerHeight(),
                drg_w = $drag.outerWidth(),
                pos_y = $drag.offset().top + drg_h - e.pageY,
                pos_x = $drag.offset().left + drg_w - e.pageX;
            $drag.css('z-index', 1000).parents().on("mousemove", function(e) {
                $('.draggable').offset({
                    top:e.pageY + pos_y - drg_h,
                    left:e.pageX + pos_x - drg_w
                }).on("mouseup", function() {
                    $(this).removeClass('draggable').css('z-index', z_idx);
                });
            });
            e.preventDefault(); // disable selection
        }).on("mouseup", function() {
            if(opt.handle === "") {
                $(this).removeClass('draggable');
            } else {
                $(this).removeClass('active-handle').parent().removeClass('draggable');
            }
        });

    }
})(jQuery);

$(document).ready(function()
{
    var div = '<div class="responsive-guide" style="position: absolute; cursor:pointer; opacity:.8; box-shadow: 5px 5px 10px #111; padding:10px; border-radius: 10px; top:10px; right:10px; width:120px; height:auto;  background:#333; font-size:10px; text-align: center; color:#fff;"><p>Container: <span class="container-guide"></span></p><p>Window: <span class="window-guide"></span></p><p>Color: <span class="class-guide"></span></p><p class="close-responsive-guide btn btn-primary btn-sm">Close</p></div>';
    div = $(div);

    $('body').append(div);

    $(div).mouseenter(function()
    {
        $(div).css({opacity:1});
    });

    $(div).mouseleave(function()
    {
        $(div).css({opacity:".8"});
    });

    $(div).drags();

    $(div).find('.close-responsive-guide').click(function()
    {
        $(div).fadeOut();
    });

    $( window ).resize(function()
    {
        $('.container-guide').html($('.container').css('width'));
        $('.window-guide').html($('html').css('width'));
        $('.class-guide').html(getCurrentClass());
    });

    $(window).trigger('resize');

    function getCurrentClass()
    {
        var width = parseInt($('html').css('width'));

        if( width >= 1200 )
        {
            return 'Red';
        }

        if( width >= 992 && width <= 1199)
        {
            return 'Forest Green';
        }

        if( width >= 768 && width <= 991 )
        {
            return 'Yellow';
        }

        if( width <= 767)
        {
            var color = 'Blue';
        }

        if( width <= 640)
        {
            var color = 'Brown';
        }

        if( width <= 568)
        {
            var color = 'Purple';
        }

        if( width <= 480)
        {
            var color = 'Green';
        }

        if( width <= 460)
        {
            var color = 'Orange';
        }


        return color;
    }
});